import React from 'react';
import './App.css';

class CountryComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            countryList: [],
        }
    }

    componentDidMount() {
        if (this.props.location.state !== undefined) {
            this.setState({
                countryList: this.props.location.state.data,
            })
        }
    }
    onItemClick = (item) => {
         this.props.history.push(
             {
                 pathname:'/weather',
                 state:{
                    capital:item.capital,
                 }
             }
         );
    }



    render() {
        return (
            <div className="center-child max-width">
                {
                    this.state.countryList.length > 0 ?

                        this.state.countryList.map((item, index) => {

                            return (

                                <div className="bottom-line" onClick={(e)=>this.onItemClick(item)}>
                                    <span className="p-10 self-center" >{item.capital}</span>
                                    <span className="p-10" >{item.population}</span>
                                    <span className="p-10" >{"lat - "+item.latlng[0] +"lan - "+item.latlng[1]}</span>
                                    <img className="p-10" height={50} width={50} alt={index} src={item.flag} />

                                </div>
                            )
                        })

                        :

                        ""

                }
            </div>
        )
    }

}

export default CountryComponent;