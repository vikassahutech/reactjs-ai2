import React from 'react';
import './App.css';

var accessKey = "3aa82ec13c808c44c809ac40b65a89e2";
class WeatherComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            data: "",
            capitalName: "",
            errorData: ""
        }
    }

    componentDidMount() {
        if (this.props.location.state !== undefined) {
            this.setState({
                capitalName: this.props.location.state.capital,
            }, () => {
                this.getWeatherDetail(this.state.capitalName);
            });


        }
    }

    getWeatherDetail(capital) {
        fetch("http://api.weatherstack.com/current?access_key=" + accessKey + "&query=" + capital).then(response => response.json())
            .then((response) => {
                if (!response.success) {
                    if (response.current !== undefined && response.current !== null) {

                        this.setState({ data: response.current, errorData: "" });
                    }else{
                    this.setState({ errorData: response.error });
                    }
                } else {
                    this.setState({ errorData: response.error });
                }
            });
    }
    render() {

        return (
            <div>
                {
                    this.state.data ?
                        <div>
                            <div> <span className="p-10 self-center" >Tempreture - {this.state.data.temperature}</span></div>
                            <div><span className="p-10" > Wind Speed - {this.state.data.wind_speed}</span></div>
                            <div><span className="p-10" >Precip - {this.state.data.precip}</span></div>
                            <div><img alt="abcd" className="p-10" src={this.state.data.weather_icons[0]} /></div>
                        </div>
                        :
                        <div>
                            <div><span className="p-10" > {this.state.errorData.info}</span></div>
                        </div>
                }
            </div>
        )
    }

}

export default WeatherComponent;