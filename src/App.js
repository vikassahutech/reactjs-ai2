import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import CountryComponent from './CountryComponent';
import WeatherComponent from './WeatherComponent';
import HomePage from './HomePage';

function App() {
  return (
    <BrowserRouter>
    <Switch>
      <Route exact path="/" component={HomePage}/>
      <Route  path="/weather" component={WeatherComponent}/>
      <Route  path="/countrylist" component={CountryComponent}/>
    </Switch>
    </BrowserRouter>
  );
}

export default App;
