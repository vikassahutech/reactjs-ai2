import React from 'react';
import './App.css';

class HomePage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            countryName: "",
            countryList: [],
            error:"",
        }
    }

    onButtonClick = () => {
        this.setState({error:""},()=>{
            this.searchCountryNameWise();
        });
    }

    searchCountryNameWise() {
        fetch("https://restcountries.eu/rest/v2/name/" + this.state.countryName).then(response => response.json())
            .then((response) => {
                if (response.status !== 404) {
                    this.setState({ countryList: response });
                    this.props.history.push(
                        {
                            pathname: '/countrylist',
                            state: {
                                data: this.state.countryList,
                            }
                        }
                    );
                }else{
                    this.setState({error:response.message});
                }
            })
    }

    render() {
        return (
            <div>
                <form typ>
                    <div>
                        <input placeholder="Enter country" onChange={(e) => this.setState({ countryName: e.target.value })}></input>
                    </div>

                    <div>
                        <button disabled={!this.state.countryName} type="button" onClick={(e) => this.onButtonClick(e)}>Button</button>
                    </div>
                </form>
               
                {this.state.error?
                <div><span>Error {this.state.error}</span></div>:""}
            </div>
        )
    }

}

export default HomePage;